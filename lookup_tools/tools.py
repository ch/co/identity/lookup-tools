#!/usr/bin/python3
import os

import requests
import yaml

auth_api_url = "https://api.apps.cam.ac.uk/oauth2/v1"
lookup_api_url = "https://api.apps.cam.ac.uk/lookup/v1"

headers = {}
headers["Accept"] = "application/json"


def lookup_login():
    settings = get_settings()
    form_data = {
        "grant_type": "client_credentials",
        "client_id": settings["lookup_client_id"],
        "client_secret": settings["lookup_client_secret"],
    }
    response = requests.post(auth_api_url + "/token", data=form_data)
    if response.status_code == 200:
        access_token = response.json().get("access_token")
        headers["Authorization"] = f"Bearer {access_token}"


def get_lookup_minimal_contact_details(crsids):
    """
    Takes a list of crsids
    Returns a list of 3-element lists with crsid, visible name, email (if found)
    """
    lookup_people = []
    lookup_login()
    params = {"includeCancelled": False, "fetch": "email"}
    batches = [crsids[i : i + 100] for i in range(0, len(crsids), 100)]
    for batch in batches:
        params["crsids"] = ",".join(batch)
        response = requests.get(
            lookup_api_url + "/person/list", headers=headers, params=params
        )
        if response.status_code == 200:
            lookup_people.extend(response.json()["result"]["people"])
        else:
            break
    return [lookup_person_to_row(p) for p in lookup_people]


def lookup_person_to_row(person):
    # There must be a crsid because we asked for the person by crsid
    # but there need not be an email. visibleName always has a value, although it
    # might just be the crsid if the person hid all their other names
    crsid = person["identifier"]["value"]
    try:
        lookup_email = [
            a["value"] for a in person["attributes"] if a["scheme"] == "email"
        ][0]
    except IndexError:
        lookup_email = None
    return [crsid, person["visibleName"], lookup_email]


def get_settings():
    settings = {}
    base_dir = os.path.dirname(__file__)
    etc_dir = "/etc/lookup-tools"
    config_filenames = [
        os.path.join(base_dir, "default.yml"),  # app defaults
        os.path.join(base_dir, "config.yml"),  # for devel
        os.path.join(etc_dir, "config.yml"),  # for deployment
    ]
    for f in config_filenames:
        if os.path.exists(f):
            with open(f, "r") as conf:
                config = yaml.load(conf, Loader=yaml.SafeLoader)
                settings.update(config)
    return settings
